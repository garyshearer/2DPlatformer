using UnityEngine;
using System.Collections;

public class CameraCollisionController : MonoBehaviour {

	public CameraCollisionController () {}

	void OnTriggerEnter2D(Collider2D coll) {
		//if (coll.ToString() == "Player"){
		Camera mainCamera = Camera.main;//coll.gameObject.GetComponentInParent<Camera> ();

		if (mainCamera == null)
			return;
		Debug.Log ("collider " + coll.ToString ());

		if (tag == "CameraSmoothFollowEnable")
			mainCamera.SendMessage ("SetScriptEnabled", true);
		else if (tag == "CameraSmoothFollowDisable")
			mainCamera.SendMessage ("SetScriptEnabled", false);
		
		//}
	}
}
