﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

	public Transform[] backgrounds;		// Array (list) of all the backgrounds
	private Vector2[] parallaxScales;		// The proportions of the movement of the backgrounds x,y
	public float smoothing = 1f;			// How smooth the parralax will be
	public float ySmoothing = 20.0f;

	private Transform cam;				// reference to the main camera transform
	private Transform player;
	private Vector3 previousCamPos;		// the position of the camera in the previous frame
	private Vector3 previousSpritePos;

	// Called before Start()
	void Awake () {
		GameObject playerCheck;
		// set up the camera reference
		cam = Camera.main.transform;
		playerCheck = GameObject.FindWithTag ("Player");
		if (playerCheck == null) 
			Debug.Log ("player object not found");
		else
			player = playerCheck.transform;
	}

	// Use this for initialization
	void Start () {

		parallaxScales = new Vector2[backgrounds.Length];

		// The previous frame had the current frame's camera position
		previousCamPos = cam.position;
		previousSpritePos = player.transform.position;

		for (int i = 0; i < backgrounds.Length; i++) {
			parallaxScales[i].x = backgrounds[i].position.z * -1;
			parallaxScales[i].y = backgrounds[i].position.z * -0.6f;
		}
	}
	// TODO: vertical parallaxing
	// verticalScaling -- Tight vertical scaling
	// Update is called once per frame
	void Update () {
		// for each background
		for (int i = 0; i < backgrounds.Length; i++) {
			// the parallax is the opposite of the camera movement because the previous frame multiplied by the scale
			float parallax = (previousCamPos.x - cam.position.x) * parallaxScales [i].x;
			float yParallax = (previousCamPos.y - cam.position.y) * parallaxScales [i].y;
			// set a target x position which is the current position plus the parallax
			float backgroundTargetPosX = backgrounds[i].position.x + parallax;
			float backgroundTargetPosY = backgrounds[i].position.y + yParallax;

			// create a target position which is the background's current position with it's target x position
			Vector3 backgroundTargetPos = new Vector3 (backgroundTargetPosX, backgroundTargetPosY, backgrounds[i].position.z);

			// fade between current position and the target position using lerp
			backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);

			// create a target position which is the background's current position with it's target x position
			//backgroundTargetPos = new Vector3 (backgrounds[i].position.x, backgroundTargetPosY, backgrounds[i].position.z);

			//backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, ySmoothing * Time.deltaTime);
			//backgrounds[i].position = new Vector3(backgrounds[i].position.x, backgroundTargetPosY, backgrounds[i].position.z) - new Vector3(0.0f, 0.8f, 0.0f);
		}

		// set the previousCamPos to the camera's position at the end of the frame
		previousCamPos = cam.position;
	}
}
