using UnityEngine;
using System.Collections;

public class MegaController : MonoBehaviour {

	enum MegaState {
		Teleport,
		Idle,
		Run,
		Falling,
		Jump,
		Landing,
		ShootRun,
		FatiguedIdle,
		Shoot,
		Blink
	}
	
	MegaState currentMegaState;
	ArrayList currentGroundCollisionList = new ArrayList();

	public float maxSpeed = 10.0f;
	bool bPreviousJumpButtonPressed = false;
	bool facingRight = true;
	//bool bOnGroundCollision = true;
	
	void OnCollisionExit2D(Collision2D coll){
		Debug.Log ("removing " + coll.gameObject.tag);
		//currentGroundCollisionList.Remove (coll);
		currentGroundCollisionList.Clear ();
	}

	void OnCollisionEnter2D(Collision2D coll){
		currentGroundCollisionList.Add (coll);

		Camera mainCamera = Camera.main;//coll.gameObject.GetComponentInParent<Camera> ();
		
		if (mainCamera == null)
			return;
		//Debug.Log ("List size: " + currentGroundCollisionList.Count);
	}

	// Use this for initialization
	void Start () {
	}


	
	private bool IsOnGround() {
        // TODO: make distinction between bottom and side collision
		return (currentGroundCollisionList.Count != 0);
	}

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    // Update is called once per frame
    void /*Fixed*/Update () {

		float move = Input.GetAxisRaw ("Horizontal");

        //  GetAxisRaw returns -1 and 1.  stopCoEff ensures we multiply by a decimal to avoid bad behavior.



		if(Input.GetKeyDown( KeyCode.Q ))
			Debug.Log(currentGroundCollisionList.Count);

        if (IsOnGround())
        {
            GetComponentInChildren<Animator>().SetBool("bFalling", false);
            GetComponentInChildren<Animator>().SetBool("bJumped", false);
        }

        if (move != 0.0f)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(move*0.005f, 0.0f));       //  Unity standard is to use AddForce for all scene nodes
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Clamp(GetComponent<Rigidbody2D>().velocity.x, -0.00005f, 0.00005f), GetComponent<Rigidbody2D>().velocity.y); // Limit character velocity
            //if (GetComponent<Rigidbody2D>().velocity.x > 0.00005f)          //Limit the velocity of mega (The right way???)
                //GetComponent<Rigidbody2D>().AddForce(new Vector2(-move * 0.0045f, 0.0f));
                //GetComponent<Rigidbody2D>().velocity = new Vector2(0.05f, GetComponent<Rigidbody2D>().velocity.y);
        }

        const float moveScaleCoEff = 0.1f;
        float velocity = (move * moveScaleCoEff) * maxSpeed;
        GetComponentInChildren<Animator>().SetFloat("fVelocity", Mathf.Abs(velocity));// TODO: change to GetComponent<Rigidbody2D>().velocity.x.  NEED COMMON UNITS!

        // Handle jump
        if (IsOnGround () && Input.GetButtonDown ("Jump")) {
			Debug.Log ("Jumped");
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0.0f, 0.016f));
			GetComponentInChildren<Animator>().SetBool ("bIsOnGround", false);
            GetComponentInChildren<Animator>().SetBool("bJumped", true);

			//GetComponentInChildren<Animator> ().SetFloat ("fYVelocity", Mathf.Abs (velocity));
		} else if (IsOnGround ()) {
			GetComponentInChildren<Animator>().SetBool ("bIsOnGround", true);
            GetComponentInChildren<Animator>().SetBool ("bJumped", false);
        } else if (!IsOnGround () && GetComponent<Rigidbody2D> ().velocity.y < 0.0f) {
			GetComponentInChildren<Animator> ().SetBool ("bFalling", true);
		} 



		if (move > 0 && !facingRight)
			Flip ();
		else if (move < 0 && facingRight)
			Flip ();

	}
}
/*
const float stopCoEff = 0.1f;
float velocity = (move*stopCoEff)*maxSpeed;
Vector2 position = new Vector2(0.0f, 0.0f);
//GetComponent<Rigidbody2D> ().velocity = new Vector2 (move*0.1f * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

// TODO: replace GetComponent with some sort of 'GetByTag' method
if(move != 0.0f)
    position = new Vector2(GetComponent<Rigidbody2D> ().position.x + velocity * Time.deltaTime,
                           GetComponent<Rigidbody2D> ().position.y);
else
    position = new Vector2(GetComponent<Rigidbody2D> ().position.x,
                           GetComponent<Rigidbody2D> ().position.y);
*/
//GetComponent<Rigidbody2D> ().position = position;

//GetComponentInChildren<Animator> ().rootPosition= position;
//if (move == 0.0f)
//	GetComponent<Rigidbody2D> ().velocity = new Vector2 (0.0f, GetComponent<Rigidbody2D>().velocity.y);