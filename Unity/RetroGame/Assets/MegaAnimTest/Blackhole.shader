﻿Shader "Custom/Blackhole" {
	Properties {
		//_Color ("Color", Color) = (1,1,1,1)
		//_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_MainTex("Texture", 2D) = "tex10.png" {}
		_BG("Texture", 2D) = "tex10.png" {}
		//_Glossiness ("Smoothness", Range(0,1)) = 0.5
		//_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		//Tags { "RenderType"="Opaque" }
		//LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard //fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BG;

		struct Input {
			float2 uv_MainTex;
		};

		//half _Glossiness;
		//half _Metallic;
		//fixed4 _Color;

		const float pi = 3.1415927;

		float sdSphere(float3 p, float s)
		{
			return length(p) - s;
		}

		float sdCappedCylinder(float3 p, float2 h)
		{
			float2 d = abs(float2(length(p.xz), p.y)) - h;
			return min(max(d.x, d.y), 0.0) + length(max(d, 0.0));
		}

		float sdTorus(float3 p, float2 t)
		{
			float2 q = float2(length(p.xz) - t.x, p.y);
			return length(q) - t.y;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);// *_Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic;
			//o.Smoothness = _Glossiness;
			o.Alpha = 1;// c.a;

			float2 pp = IN.uv_MainTex;// fragCoord.xy / iResolution.xy;
			pp = -1.0 + 2.0*pp;
			pp.x *= _ScreenParams.x / _ScreenParams.y;

			float3 lookAt = float3(0.0, -0.1, 0.0);

			float eyer = 2.0;
			float eyea = /*(iMouse.x / iResolution.x)*/0.5 * pi * 1.0;
			float eyea2 = /*((iMouse.y / iResolution.y)*/(0.5 - 0.222) * pi * 1.0;

			float3 ro = float3(
				eyer * cos(eyea) * sin(eyea2),
				eyer * cos(eyea2),
				eyer * sin(eyea) * sin(eyea2)); //camera position


			float3 front = normalize(lookAt - ro);
			float3 left = normalize(cross(normalize(float3(0.0, 1, -0.1)), front));
			float3 up = normalize(cross(front, left));
			float3 rd = normalize(front*1.5 + left*pp.x + up*pp.y); // rect vector


			float3 bh = float3(0.0, 0.0, 0.0);
			float bhr = 0.1;
			float bhmass = 5.0;
			bhmass *= 0.001; // premul G

			float3 p = ro;
			float3 pv = rd;
			float dt = 0.02;

			float3 col = float3(0.0, 0.0, 0.0);

			float noncaptured = 1.0;

			float3 c1 = float3(0.5, 0.35, 0.1);
			float3 c2 = float3(1.0, 0.8, 0.6);


			for (float t = 0.0; t<1.0; t += 0.005)
			{
				p += pv * dt * noncaptured;

				// gravity
				float3 bhv = bh - p;
				float r = dot(bhv, bhv);
				pv += normalize(bhv) * ((bhmass) / r);

				noncaptured = smoothstep(0.0, 0.666, sdSphere(p - bh, bhr));

				// Texture for the accretion disc
				float dr = length(bhv.xz);
				float da = atan2(bhv.x, bhv.z);
				float2 ra = float2(dr, da * (0.01 + (dr - bhr)*0.002) + 2.0 * pi + /*iGlobalTime*/_Time.y*0.002);
				ra *= float2(10.0, 20.0);

				float3 dcol = lerp(c2, c1, pow(length(bhv) - bhr, 2.0)) * max(0.0, tex2D(_MainTex, ra*float2(0.1, 0.5)).r + 0.05) *
					(4.0 / ((0.001 + (length(bhv) - bhr)*50.0)));

				col += max(float3(0.0, 0.0, 0.0), dcol * smoothstep(0.0, 1.0, -sdTorus((p * float3(1.0, 25.0, 1.0)) - bh, float2(0.8, 0.99))) * noncaptured);

				//col += dcol * (1.0/dr) * noncaptured * 0.01;

				// Glow
				float bhvs = dot(bhv, bhv);
				col += float3(1.0, 0.9, 0.85) * (1.0 / float3(bhvs, bhvs, bhvs)) * 0.0033 * noncaptured;

			}

			// BG
			col += pow(tex2D(_BG, pv.xy + float2(1.5, 1.5)).rgb, float3(3.0, 3.0, 3.0));
			//float3 texColor = tex2D(_BG, pv.xy + float2(1.5, 1.5));
			//col = col + texColor;// (float3(pow(pow(pow(texColor.r))), pow(pow(pow(texColor.g))), pow(pow(pow(texColor.b)))));
			// Final color
			o.Albedo = col;
		}

		// My edit of https://www.shadertoy.com/view/XdjXDy
		// So yeah, thank bloodnok for this brilliant shader, not me
		// The original one just had some visual problems which I corrected
		// Or I should probably say; 'corrected' to fit my own taste
		// So don't praise me, praise bloodnok
/*
		

		void mainImage(out vec4 fragColor, in vec2 fragCoord)
		{

			
			vec2 pp = fragCoord.xy / iResolution.xy;
			pp = -1.0 + 2.0*pp;
			pp.x *= iResolution.x / iResolution.y;

			vec3 lookAt = vec3(0.0, -0.1, 0.0);

			float eyer = 2.0;
			float eyea = (iMouse.x / iResolution.x) * pi * 2.0;
			float eyea2 = ((iMouse.y / iResolution.y) - 0.222) * pi * 2.0;

			vec3 ro = vec3(
				eyer * cos(eyea) * sin(eyea2),
				eyer * cos(eyea2),
				eyer * sin(eyea) * sin(eyea2)); //camera position


			vec3 front = normalize(lookAt - ro);
			vec3 left = normalize(cross(normalize(vec3(0.0, 1, -0.1)), front));
			vec3 up = normalize(cross(front, left));
			vec3 rd = normalize(front*1.5 + left*pp.x + up*pp.y); // rect vector


			vec3 bh = vec3(0.0, 0.0, 0.0);
			float bhr = 0.1;
			float bhmass = 5.0;
			bhmass *= 0.001; // premul G

			vec3 p = ro;
			vec3 pv = rd;
			float dt = 0.02;

			vec3 col = vec3(0.0);

			float noncaptured = 1.0;

			vec3 c1 = vec3(0.5, 0.35, 0.1);
			vec3 c2 = vec3(1.0, 0.8, 0.6);


			for (float t = 0.0; t<1.0; t += 0.005)
			{
				p += pv * dt * noncaptured;

				// gravity
				vec3 bhv = bh - p;
				float r = dot(bhv, bhv);
				pv += normalize(bhv) * ((bhmass) / r);

				noncaptured = smoothstep(0.0, 0.666, sdSphere(p - bh, bhr));

				// Texture for the accretion disc
				float dr = length(bhv.xz);
				float da = atan(bhv.x, bhv.z);
				vec2 ra = vec2(dr, da * (0.01 + (dr - bhr)*0.002) + 2.0 * pi + iGlobalTime*0.002);
				ra *= vec2(10.0, 20.0);

				vec3 dcol = mix(c2, c1, pow(length(bhv) - bhr, 2.0)) * max(0.0, texture2D(iChannel1, ra*vec2(0.1, 0.5)).r + 0.05) * (4.0 / ((0.001 + (length(bhv) - bhr)*50.0)));

				col += max(vec3(0.0), dcol * smoothstep(0.0, 1.0, -sdTorus((p * vec3(1.0, 25.0, 1.0)) - bh, vec2(0.8, 0.99))) * noncaptured);

				//col += dcol * (1.0/dr) * noncaptured * 0.01;

				// Glow
				col += vec3(1.0, 0.9, 0.85) * (1.0 / vec3(dot(bhv, bhv))) * 0.0033 * noncaptured;
				
			}

			// BG
			col += pow(texture2D(iChannel0, pv.xy + vec2(1.5)).rgb, vec3(3.0));

			// FInal color
			fragColor = vec4(col, 1.0);
		}*/
		ENDCG
	} 
	FallBack "Diffuse"
}
